// Fill out your copyright notice in the Description page of Project Settings.


#include "Platform_Jump.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

#include <Engine.h>

// Sets default values
APlatform_Jump::APlatform_Jump()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//sceneRoot;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root_platform"));
	RootComponent = Root;
	Root->SetupAttachment(RootComponent);//sino agregas esto no vas a ver las propiedades de transform position scale


	//component_platform
	mesh_platform = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh_platform"));
	mesh_platform->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	mesh_platform->SetupAttachment(RootComponent);//sino agregas esto no vas a ver las propiedades de transform position scale


	//Box_Trigger
	Box_Jump = CreateDefaultSubobject<UBoxComponent>(TEXT("Box_Jump"));
	Box_Jump->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	Box_Jump->SetupAttachment(RootComponent);//sino agregas esto no vas a ver las propiedades de transform position scale


}

// Called when the game starts or when spawned
void APlatform_Jump::BeginPlay()
{
	Super::BeginPlay();

	if (Box_Jump == nullptr) return;
	Box_Jump->OnComponentBeginOverlap.AddDynamic(this, &APlatform_Jump::on_component_end_overlap_Jump);
}

// Called every frame
void APlatform_Jump::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlatform_Jump::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlatform_Jump::on_component_end_overlap_Jump(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Algo_entro al area"));
}

