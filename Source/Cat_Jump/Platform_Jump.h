// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Platform_Jump.generated.h"

UCLASS()
class CAT_JUMP_API APlatform_Jump : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlatform_Jump();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UPROPERTY(VisibleAnywhere)USceneComponent* Root {nullptr};




	
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* mesh_platform{ nullptr };

//trigger jump
protected:
	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* Box_Jump;

	UFUNCTION()
	void on_component_end_overlap_Jump(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	





};
