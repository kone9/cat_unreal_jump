// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Cat_JumpGameMode.generated.h"

UCLASS(minimalapi)
class ACat_JumpGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACat_JumpGameMode();
};



