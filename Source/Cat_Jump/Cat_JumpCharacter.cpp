// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Cat_JumpCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

//#include <Engine.h>
#include "Components/BoxComponent.h"

ACat_JumpCharacter::ACat_JumpCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	//CameraBoom->bAbsoluteRotation = true; // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	//CameraBoom->RelativeRotation = FRotator(0.f,180.f,0.f);

	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)


	//Box_Trigger Jump
	//Box_Jump = CreateDefaultSubobject<UBoxComponent>(TEXT("Box_Jump"));
	//Box_Jump->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	//Box_Jump->SetupAttachment(RootComponent);//sino agregas esto no vas a ver las propiedades de transform position scale
}



//void ACat_JumpCharacter::BeginPlay()
//{
//	Super::BeginPlay();
//
//	if (Box_Jump == nullptr) return;
//	Box_Jump->OnComponentBeginOverlap.AddDynamic(this, &ACat_JumpCharacter::on_component_begin_overlap_Jump);
//}
//
//void ACat_JumpCharacter::on_component_begin_overlap_Jump(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
//{
//	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("LA COLISION DEL PERSONAJE TAMBIE DETECTO ALGO"));
//}


//////////////////////////////////////////////////////////////////////////
// Input

void ACat_JumpCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACat_JumpCharacter::MoveRight);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &ACat_JumpCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ACat_JumpCharacter::TouchStopped);
}

void ACat_JumpCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f,-1.f,0.f), Value);
	AddControllerYawInput(Value * rotation_velocity);
}

void ACat_JumpCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void ACat_JumpCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}

