// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Generador_Plataformas.generated.h"

UCLASS()
class CAT_JUMP_API AGenerador_Plataformas : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGenerador_Plataformas();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


public:
	//cantidad a instanciar
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int number_platform{ 200 };

public:
	//posiciones para instanciar
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float min_z{100};
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float max_z{600};

	//posiciones para instanciar
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float min_y{ -290 };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float max_y{ 300 };
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float level_ancho{ 0 };

public:
	//tipo de objeto a instanciar
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> platform;

private:
	void instanciar_plataforma(FVector new_position, TSubclassOf<AActor> new_platform);


};
