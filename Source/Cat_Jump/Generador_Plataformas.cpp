// Fill out your copyright notice in the Description page of Project Settings.


#include "Generador_Plataformas.h"
#include <Engine/World.h>
//#include <Engine.h>
#include "Kismet/KismetMathLibrary.h"


// Sets default values
AGenerador_Plataformas::AGenerador_Plataformas()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGenerador_Plataformas::BeginPlay()
{
	Super::BeginPlay();
	
	FVector spawn_position{ FVector::ZeroVector };

	for (int i = 0; i < number_platform; i++)
	{
		spawn_position.Z += UKismetMathLibrary::RandomFloatInRange(min_z, max_z);
		spawn_position.Y = UKismetMathLibrary::RandomFloatInRange(min_y, max_y);
		instanciar_plataforma(spawn_position, platform);
	}
}


// Called every frame
void AGenerador_Plataformas::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AGenerador_Plataformas::instanciar_plataforma(FVector new_position, TSubclassOf<AActor> new_platform)
{
	if (new_platform == nullptr) return;
	// Don't forget to include World.h to use methods inside UWorld Class
	FActorSpawnParameters spawn_params{};
	spawn_params.Owner = nullptr; // Specify owner actor for this new actor if you need one.
	spawn_params.Instigator = nullptr; // Specify instigator pawn for this new actor if you need one
	spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;


	FTransform new_actor_transform = FTransform::Identity; // Identity Matrix if you need the default transformation.

	new_actor_transform.SetLocation( FVector(new_position));

	// Spawn Actor return a Pointer to new Actor
	if (GetWorld() == nullptr) return;
	GetWorld()->SpawnActor<AActor>(new_platform, new_actor_transform, spawn_params);

	
}

